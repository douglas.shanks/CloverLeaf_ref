#!/bin/bash

#fortran kernels
for i in {1..2}
do
  printf "Running CloverLeaf Benchmark $i\n"
  cp InputDecks/clover_bm_$i.in clover.in
  
  OMP_NUM_THREADS=1 mpirun ./clover_leaf
  
  if grep -1 "NOT PASSED" clover.out; then
    printf "CloverLeaf Benchmark $i Failed\n"
    exit 1
  else
    printf "CloverLeaf Benchmark $i Passed\n"
  fi
done

#C kernels
for i in {1..2}
do
  printf "Running CloverLeaf Benchmark $i\n"
  cp InputDecks/clover_bm_c_$i.in clover.in
  
  OMP_NUM_THREADS=1 mpirun ./clover_leaf
  
  if grep -1 "NOT PASSED" clover.out; then
    printf "CloverLeaf Benchmark $i Failed\n"
    exit 1
  else
    printf "CloverLeaf Benchmark $i Passed\n"
  fi
done
